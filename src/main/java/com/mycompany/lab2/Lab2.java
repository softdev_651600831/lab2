/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;


import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Lab2 {
    
    static char [][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char Player ='X';
    static int row,col;
   
    
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    
    static void printTable(){
        
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    static void setTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
               table[i][j] = '-';
            }
        }
    }
    
    
    static void printTurn(){
        System.out.println(Player+" Turn");
    }
    
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col -1] =='-'){
                table[row-1][col -1] = Player;
                break;
            }
        }
    }
    static void switchPlayer(){
   
        if (Player == 'X'){
            Player = 'O';
        }
        else{
            Player = 'X';
        }
    }
    
     static void printWin(){
        System.out.println(Player+" Win!!");
     }
     
    static void printDraw(){
        System.out.println("Draw!!");
     }
     
    static void printEnd(){
        System.out.println("End game");
       
     }
    
    static boolean isWin(){
       if(checkCol()|| checkRow()||checkX1()||checkX2()){
        return true;
        }
        return false;
    }
    
    static boolean isDraw(){
       for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                return false;
                }
            }
        }
        return true;
    }
    
    static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[row -1 ][i]!=Player){
            return false;
            }
        }
        return true;
    }
    
    static boolean checkCol(){
        for(int j=0;j<3;j++){
            if(table[j ][col-1]!=Player){
            return false;
            }
        }
        return true;
    }
    
    static boolean checkX1(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][i]!=Player){
                return false;
                }
            }
        }
        return true;
    }
    
    static boolean checkX2(){
        for(int i=0;i<3;i++){
            if((table[0][2]==Player) && (table[1][1]==Player) && (table[2][0]==Player)) {
                return true;
            }
        }
        return false;
    }
    
    static char cont = 'y' ;
       static void prinCont(){
            System.out.print("Continue (y/n): ");
            Scanner sc = new Scanner(System.in);
            char c = sc.next().charAt(0);  
            cont = c;
      }
       
      
      static boolean checkCont(){
        if(cont=='y'){
            return true;
            }
          return false; 
      }
     
 public static void main(String[] args) {
 
        printWelcome(); 
      while(checkCont()){ 
           printTable();
           printTurn();
           inputRowCol();

        if(isWin()){
            printTable();
            printWin();
            setTable();
            prinCont();

        }
         if(isDraw()){
              printTable();
              printDraw();
              setTable();
              prinCont(); 
        }
        
        switchPlayer();
                
        }
      printEnd();
      
    }

}
